# mxGraph-demo

#### 介绍
mxGraph-demo是使用mxGraph和ztree完成在线编辑配置网络拓扑架构图的演示案例。
由于项目需要，本想从网上找些jQuery拓扑图的插件完成功能开发，但居然木有...
也找了一些其他的框架，如jGraph、D3等等组件，但有些不符合要求，有些需要商业化授权，最后选择了这个mxGraph。
演示请看 [https://caolj.gitee.io/mxgraph-demo](https://caolj.gitee.io/mxgraph-demo)}

#### 软件架构

1.  jQuery v1.12.4
2.  mxGraph 4.0.5
3.  jquery-easyui-1.8.1
4.  zTree_v3.5.40



#### 其他说明

1.  mxGraph没有中文API，官网的文档感觉又不是很丰富
2.  本Demo没有和后台数据交互，存取数据只在前端
3.  很多坑都已经在代码里注释，希望有帮助
