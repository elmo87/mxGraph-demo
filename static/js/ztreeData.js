var zNodes = [{
					id: 1,
					pId: 0,
					name: "待配置项",
					open: true
				},
				{
					id: 11,
					pId: 1,
					name: "网络设备",
					open: true
				},
				{
					id: 111,
					pId: 11,
					name: "网络设备1",
					iconSkin: 'server'
				},
				{
					id: 112,
					pId: 11,
					name: "网络设备2",
					iconSkin: 'server'
				},
				{
					id: 12,
					pId: 1,
					name: "动环设备",
					open: true
				},
				{
					id: 121,
					pId: 12,
					name: "动环设备1",
					iconSkin: 'server'
				},
				{
					id: 122,
					pId: 12,
					name: "动环设备2",
					iconSkin: 'server'
				},
				{
					id: 13,
					pId: 1,
					name: "强电线缆",
					open: true
				},
				{
					id: 131,
					pId: 13,
					name: "强电线缆一",
					iconSkin: 'line'
				},
				{
					id: 132,
					pId: 13,
					name: "强电线缆二",
					iconSkin: 'line'
				},
				{
					id: 133,
					pId: 13,
					name: "强电线缆三",
					iconSkin: 'line'
				},
				{
					id: 134,
					pId: 13,
					name: "强电线缆四",
					iconSkin: 'line'
				},
				{
					id: 14,
					pId: 1,
					name: "弱电线缆",
					open: true
				},
				{
					id: 141,
					pId: 14,
					name: "弱电线缆一",
					iconSkin: 'line'
				},
				{
					id: 142,
					pId: 14,
					name: "弱电线缆二",
					iconSkin: 'line'
				},
				{
					id: 143,
					pId: 14,
					name: "弱电线缆三",
					iconSkin: 'line'
				},
				{
					id: 144,
					pId: 14,
					name: "弱电线缆四",
					iconSkin: 'line'
				},
				{
					id: 4,
					pId: 1,
					name: "配线架",
					open: true
				},
				{
					id: 41,
					pId: 4,
					name: "配线架一",
					iconSkin: 'exchanger'
				},
				{
					id: 42,
					pId: 4,
					name: "配线架二",
					iconSkin: 'exchanger'
				}
			];